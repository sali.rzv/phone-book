<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <script src="{{asset('js/jq.js')}}"></script>
    <title>پنل مدیریت</title>
</head>
<body>
<?php

use App\Phone;use App\User;use Illuminate\Support\Facades\Auth;$admin = Auth::user();
$users = User::all();
$phones = Phone::all();

?>
<div class="admin-panel-container">
    <div class="admin-panel-nav">
        <div class="admin-panel-nav-header">
            <p>خوش آمدید ، مدیر ، {{$admin->name}}</p>
        </div>
        <div class="admin-panel-nav-body">
            <ul>
                <li id="dashboard"><a href="{{route('adminpanelmain')}}">پیشخوان</a></li>
                <li id="admin-users"><a href="{{route('adminpanelusers')}}">کاربران</a></li>
                <li id="phone-numbers"><a href="{{route('adminpanelphones')}}">شماره ها</a></li>
            </ul>
        </div>
        <div class="menu-toggler">
            بستن داشبورد
        </div>
    </div>
    <div class="admin-panel-body">
        <div class="dashboard">
            <p class="count">تا کنون <span class="red">{{$users->count()}}</span> کاربر عضو شده است</p>
            <p class="count">تا کنون <span class="red">{{$phones->count()}}</span> شماره ثبت شده است</p>
            <p class="content"><a href="{{route('logout')}}">خروج</a></p>
        </div>
    </div>
</div>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>
