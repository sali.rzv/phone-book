<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>پنل کاربری</title>
</head>
<body>
<?php
use Illuminate\Support\Facades\Auth;
$user = App\User::find(Auth::id());
$phones = $user->phones->all();
?>
<div class="user-panel-container">
    <div class="user-panel-header">
        <p>خوش آمدید ، {{$user->name}}</p>
        <a href="{{route('logout')}}">خروج</a>
    </div>
    <div class="user-panel-body">
        <div class="phone-counts">
            <p>شما <span class="strong">
                    {{$user->phones()->count()}}
                </span> شماره در دفتر خود وارد کرده اید</p>
        </div>
        <div class="add-phone">
            <div class="add-phone-header">
                <p>افزودن شماره جدید</p>
            </div>
            <div class="add-phone-body">
                <form action="{{route('add_phone')}}" method="POST">
                    @csrf
                    <label for="number">شماره</label>
                    <input type="text" id="number" name="phonenumber" autocomplete="off">
                    <input type="hidden" value="{{$user->id}}" name="user_id">
                    <button type="submit">افزودن</button>
                </form>
            </div>
        </div>
        <div class="phone-book">
            <div class="phone-book-header">
                <p>تمامی شماره های شما</p>
            </div>
            <div class="phone-book-body">
                <table>
                    <tr>
                        <th>عملیات</th>
                        <th>شماره</th>

                    </tr>
                    <?php
                    foreach ($phones as $phone){
                        ?>
                        <tr>
                        <td><a href="{{route('remove',$phone->id)}}">حذف</a></td>
                        <td><?php echo $phone->user_phone ?></td>
                    </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
