<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>صفحه اصلی</title>
    <link rel="stylesheet" href="{{asset("css/main.css")}}">
    <script src="{{asset("js/jq.js")}}"></script>

</head>
<body>
<div class="main-page-greeting">
    <h3>سلام کاربر مهمان ، شما ثبت نام نکرده اید و یا وارد حساب کاربری خود نشده اید</h3>
</div>
<div class="nav-links-container">
    <div class="nav-links">
        <div class="register">
            <a href="{{route("register")}}">
                <div class="register-image"><img src="{{asset("material/users.png")}}" alt="">
                    <div class="white-hover" id="register"></div>
                </div>
                <div class="register-text">ثبت نام</div>
            </a>

        </div>
        <div class="login">
            <a href="{{route('login')}}">
                <div class="login-image">
                    <img src="{{asset("material/enter.png")}}" alt="">
                    <div class="white-hover" id="login"></div>
                </div>
                <div class="login-text">ورود</div>
            </a>
        </div>
    </div>
</div>

</div>

<script src="{{asset("js/main.js")}}"></script>
</body>
</html>
