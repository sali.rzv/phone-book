<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <title>فرم ورود</title>
</head>
<body>
<div class="register-form">
    <div class="register-form-header">
        <p>فرم ورود</p>
    </div>
    <div class="form-fields">
        <form action="{{route('login')}}" method="POST">
            @csrf
            <label for="email">ایمیل</label>
            <input type="text" id="email" name="email" autocomplete="email" autofocus value="{{old('email')}}">
            @error('email')
            {{$message}}
            @enderror
            <label for="password">رمز عبور</label>
            <input type="password" id="password" name="password" autocomplete="current-password" required>
            @error('password')
            {{$message}}
            @enderror
            <div class="submit-btn-container">
                <button type="submit">ورود</button>
            </div>

        </form>
    </div>
    <div class="return-to-main">
        <a href="{{route('main')}}">بازگشت به صفحه اصلی</a>
    </div>
</div>
</body>
</html>
