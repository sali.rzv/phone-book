<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset("css/main.css")}}">
    <title>ثبت نام</title>
</head>
<body>
<div class="register-form">
    <div class="register-form-header">
        <p>فرم ثبت نام</p>
    </div>
    <div class="form-fields">
        <form action="{{route('register')}}" method="POST">
            @csrf
            <label for="name">اسم</label>
            <input type="text" id="name" name="name" autocomplete="name" required value="{{old('name')}}">
            @error('name')
            <strong>{{ $message }}</strong>
            @enderror
            <label for="email">ایمیل</label>
            <input type="email" id="email" name="email" autocomplete="email" required value="{{old('email')}}">
            @error('email')
            <strong>{{ $message }}</strong>
            @enderror
            <label for="password">رمز عبور</label>
            <input type="password" id="password" name="password" autocomplete="new-password">
            @error('password')
            <strong>{{ $message }}</strong>
            @enderror
            <label for="password-confirm">تایید رمز عبور</label>
            <input type="password" id="password-confirm" name="password_confirmation" autocomplete="new-password">
            <div class="submit-btn-container">
                <button type="submit">ثبت نام</button>
            </div>

        </form>
    </div>
    <div class="return-to-main">
        <a href="{{route('main')}}">بازگشت به صفحه اصلی</a>
    </div>
</div>
</body>
</html>
