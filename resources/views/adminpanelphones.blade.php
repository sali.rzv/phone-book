<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset("css/main.css")}}">
    <script src="{{asset("js/jq.js")}}"></script>
    <title>پنل مدیریت</title>
</head>
<body>
<?php
use App\Phone;use App\User;use Illuminate\Support\Facades\Auth;
$phones = Phone::all();
$admin = Auth::user();
?>
<div class="admin-panel-container">
    <div class="admin-panel-nav">
        <div class="admin-panel-nav-header">
            <p>خوش آمدید ، مدیر ، {{$admin->name}}</p>
        </div>
        <div class="admin-panel-nav-body">
            <ul>
                <li id="dashboard"><a href="{{route('adminpanelmain')}}">پیشخوان</a></li>
                <li id="admin-users"><a href="{{route('adminpanelusers')}}">کاربران</a></li>
                <li id="phone-numbers"><a href="{{route('adminpanelphones')}}">شماره ها</a></li>
            </ul>
        </div>
        <div class="menu-toggler">
            بستن داشبورد
        </div>
    </div>
    <div class="admin-panel-body">
        <div class="users">
            <div class="users-header">
                <p>لیست شماره ها</p>
            </div>
            <div class="users-body">
                <table>
                    <tr>
                        <th>شماره</th>
                        <th>متعلق به</th>
                        <th>عملیات</th>
                    </tr>

                    <?php
                    foreach ($phones as $phone){
                        ?>
                    <tr>
                        <td>{{$phone->user_phone}}</td>
                        <td>{{$phone->user->name}}</td>
                        <td><a href="{{route('remove',$phone->id)}}">حذف</a></td>
                    </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="{{asset("js/main.js")}}"></script>
</body>
</html>
