<?php

namespace App\Http\Controllers;

use App\Phone;
use App\User;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    public function add(Request $request)
    {
        $new_phone = new Phone();
        $new_phone->user_phone = $request->get('phonenumber');
        $new_phone->user_id = $request->get('user_id');
        $new_phone -> save();
        return redirect(route('main'));

    }

    public function remove(Request $request)
    {
        $phone = Phone::find($request->route()->parameter('id'));
        $phone->delete();
        return redirect(route('main'));
    }

    public function remove_user(Request $request)
    {
        $user = User::find($request->route()->parameter('id'));
        if ($user->is_admin!==1){
            $user->delete();
        }
        return redirect(route('adminpanelusers'));
    }
}
