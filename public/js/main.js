$(document).ready(function () {
   $(".register-image").hover(function () {
       let el = $(".white-hover#register");
       el.css("right","190%");
   },function () {
       let el = $(".white-hover#register");
       el.css("right","-95%");
   });
    $(".login-image").hover(function () {
        let el = $(".white-hover#login");
        el.css("right","190%");
    },function () {
        let el = $(".white-hover#login");
        el.css("right","-95%");
    });
    $(".menu-toggler").click(function () {
        if($(".admin-panel-nav-header").css("display")!=="none"){
            $(".admin-panel-nav").css("right","-20%");
            $(".admin-panel-nav-header").fadeToggle();
            $(".admin-panel-body").css("width","95%");
        }else{
            $(".admin-panel-nav").css("right","0");
            $(".admin-panel-nav-header").fadeToggle();
            $(".admin-panel-body").css("width","75%");
        }

    })
});