<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;


Auth::routes();

Route::get('/', function () {
    if (!Auth::check()){
        return view('main-page');
    }
    return redirect(route('userpanel'));
})->name('main');

Route::get('register',function(){
    if (!Auth::check()){
        return view("auth/user-register");
    }
    return redirect(route('userpanel'));
})->name('register');
Route::get('login',function (){
    if (!Auth::check()){
        return view('auth/user-login');
    }
    return redirect(route('userpanel'));
})->name('login');

Route::get('userpanel',function (){
   if (Auth::check()){
       if (Auth::user()->is_admin==1){
           return redirect(route('adminpanelmain'));
       }
    return view('userPanel');
   }
   return redirect(route('main'));
})->name('userpanel');
Route::get('adminpanel',function (){
    if (Auth::check()){
        if (Auth::user()->is_admin==1){
            return view('adminpanel');
        }
    }

    return 'access denied';
})->name('adminpanelmain');

Route::get('adminpanel/users',function (){
    if (Auth::check()){
        if (Auth::user()->is_admin==1){
            return view('adminpanelusers');
        }
    }
    return 'access denied';
})->name('adminpanelusers');
Route::get('adminpanel/phones',function (){
   if (Auth::check()){
       if (Auth::user()->is_admin==1){
           return view('adminpanelphones');
       }
   }
    return 'access denied';
})->name('adminpanelphones');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Route::post('/add-phone-number','PhoneController@add')->name('add_phone');
Route::get('/remove-phone-number/{id}','PhoneController@remove')->name('remove');
Route::get('/remove-user/{id}','PhoneController@remove_user')->name('remove_user');

